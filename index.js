var canvas;
var ctx;
var mode = "none";
var slider;
var tab;
var colorPicker;
var undoArray = [];
var redoArray = [];
var body;
var text_size, text_font;
var imageLoader;
var color;

let coord = {x:0 , y:0}; 
let paint = false;
let hue = 0;

$( document ).ready( function() {
    canvas = document.getElementById("myCanvas");
    ctx = canvas.getContext("2d");
    ctx.strokeStyle = "black";
    ctx.lineJoin = 'round';
    ctx.lineCap = 'round';
    ctx.fillStyle = "#F4F7FE";
    ctx.fillRect(0, 0, canvas.width, canvas.height);
    ctx.fillStyle = "black";
    color = ctx.fillStyle;

    body = document.getElementById("body");

    tab = document.getElementById("myTable");

    text_font = document.getElementById("myFont");
    text_size = document.getElementById("myFontSize");
    
    slider = document.getElementById("myRange");
    ctx.lineWidth = slider.value;

    slider.oninput = function() {   
        ctx.lineWidth = this.value;
        console.log(ctx.lineWidth);
    }

    colorPicker = document.getElementById("brushColor");
    colorPicker.addEventListener("change", watchColorPicker, false);

    function watchColorPicker(event) {
        ctx.strokeStyle = event.target.value;
        ctx.fillStyle = event.target.value;
        color = ctx.fillStyle;
    }

    imageLoader = document.getElementById("imageLoader");
    imageLoader.addEventListener('change', handleImage, false);
});

window.addEventListener('load', ()=>{
    canvas.addEventListener('mousedown', startInput);
    canvas.addEventListener('mouseup', stopInput);
    canvas.addEventListener('mousemove', sketch);
});

function change_mode(num){
    if (num == "brush"){
        mode = "brush";
        ctx.strokeStyle = color;
        ctx.fillStyle = color;
        var elementToChange = document.getElementsByTagName("body")[0];
        elementToChange.style.cursor = 'url("img/pen.cur"), auto';
    } else if (num == "eraser"){
        mode = "eraser";
        var elementToChange = document.getElementsByTagName("body")[0];
        elementToChange.style.cursor = 'url("img/eraser.cur"), auto';
    } else if (num == "text"){
        mode = "text";
        ctx.strokeStyle = color;
        ctx.fillStyle = color;
        var elementToChange = document.getElementsByTagName("body")[0];
        elementToChange.style.cursor = "text";
    } else if (num == "circle"){
        mode = "circle";
        ctx.strokeStyle = color;
        ctx.fillStyle = color;
        var elementToChange = document.getElementsByTagName("body")[0];
        elementToChange.style.cursor = "crosshair";
    } else if (num == "triangle"){
        mode = "triangle";
        ctx.strokeStyle = color;
        ctx.fillStyle = color;
        var elementToChange = document.getElementsByTagName("body")[0];
        elementToChange.style.cursor = "crosshair";
    } else if (num == "square"){
        mode = "square";
        ctx.strokeStyle = color;
        ctx.fillStyle = color;
        var elementToChange = document.getElementsByTagName("body")[0];
        elementToChange.style.cursor = "crosshair";
    } else if (num == "redo"){
        mode = "redo";
        var elementToChange = document.getElementsByTagName("body")[0];
        elementToChange.style.cursor = "default";

        if (redoArray.length > 0) {
            undoArray.push(ctx.getImageData(0, 0, canvas.width, canvas.height));
            ctx.putImageData(redoArray.pop(), 0, 0);
        }
    } else if (num == "undo"){
        mode = "undo";
        var elementToChange = document.getElementsByTagName("body")[0];
        elementToChange.style.cursor = "default";

        if (undoArray.length > 0) {
            redoArray.push(ctx.getImageData(0, 0, canvas.width, canvas.height));
            ctx.putImageData(undoArray.pop(), 0, 0);
        }
    } else if (num == "refresh"){
        mode = "refresh";
        var elementToChange = document.getElementsByTagName("body")[0];
        elementToChange.style.cursor = "default";
        
        ctx.fillStyle = "#F4F7FE";
        ctx.fillRect(0, 0, canvas.width, canvas.height);
        undoArray = [];
        redoArray = [];
    } else if (num == "rainbow"){
        mode = "rainbow";
        var elementToChange = document.getElementsByTagName("body")[0];
        elementToChange.style.cursor = 'url("img/Rainbow.cur"), auto';
        color = ctx.strokeStyle;
    }
}

function startInput(event){
    paint = true;
    undoArray.push(ctx.getImageData(0, 0, canvas.width, canvas.height));
    redoArray = [];
    coord.x = event.offsetX;
    coord.y = event.offsetY;
    if (mode == "brush" || mode == "rainbow") {
        ctx.globalCompositeOperation="source-over";
        
        ctx.beginPath();
        ctx.moveTo(event.offsetX, event.offsetY);
        ctx.lineTo(event.offsetX, event.offsetY);
        ctx.stroke();
    } else if (mode == "eraser") {
        ctx.globalCompositeOperation="destination-out";
        
        ctx.beginPath();
        ctx.moveTo(event.offsetX, event.offsetY);
        ctx.lineTo(event.offsetX, event.offsetY);
        ctx.stroke();
    } else if (mode == "text"){
        ctx.globalCompositeOperation="source-over";

        ctx.font = text_size.value + "px " + text_font.options[text_font.selectedIndex].value;
        ctx.fillText(document.getElementById("text").value, event.offsetX, event.offsetY);
    } else if (mode == "circle"){
      var rect = canvas.getBoundingClientRect();
      coord.x = event.clientX - rect.left;
      coord.y = event.clientY - rect.top;
    } else if (mode == "triangle"){
      coord.x = event.offsetX;
      coord.y = event.offsetY;
    } else if (mode == "square"){
      coord.x = event.offsetX;
      coord.y = event.offsetY;
    }
  
}

function sketch(event){
    if (!paint) return;
    if(mode == "brush"){
        ctx.lineTo(event.offsetX, event.offsetY);
        ctx.stroke();
        ctx.beginPath();
        ctx.moveTo(event.offsetX, event.offsetY);  
    }else if(mode == "eraser"){
        ctx.lineTo(event.offsetX, event.offsetY);
        ctx.stroke();
        ctx.beginPath();
        ctx.moveTo(event.offsetX, event.offsetY);  
    } else if (mode == "circle"){
        hold_pic();  
        ctx.beginPath();
        ctx.arc(coord.x, coord.y, Math.sqrt(Math.pow(event.offsetX - coord.x, 2) + Math.pow(event.offsetY - coord.y, 2)), 0, 2 * Math.PI);
        ctx.fill();
    } else if (mode == "triangle"){
        hold_pic();
        ctx.beginPath();
        ctx.moveTo(coord.x, coord.y);
        ctx.lineTo(event.offsetX, event.offsetY);
        ctx.lineTo(2 * coord.x - event.offsetX, event.offsetY);
        ctx.closePath();
        ctx.fill();
    } else if (mode == "square"){
        hold_pic();
        ctx.beginPath();
        ctx.fillRect(coord.x, coord.y, event.offsetX - coord.x, event.offsetY - coord.y);
        ctx.stroke();
    } else if (mode == "rainbow"){
        ctx.strokeStyle = `hsl(${hue}, 100%, 50%)`

        ctx.lineTo(event.offsetX, event.offsetY);
        ctx.stroke();
        ctx.beginPath();
        ctx.moveTo(event.offsetX, event.offsetY);  

        hue++
        if (hue >= 360) {
            hue = 0
        }
    }
}

function hold_pic(){
    var img;
    img = undoArray.pop();
    ctx.putImageData(img, 0, 0);
    undoArray.push(img);
}

function stopInput(event){
  paint = false;    
}

function downloadCanvas(){  
  var image = canvas.toDataURL();  

  var tmpLink = document.createElement( 'a' );  
  tmpLink.download = 'image.png';
  tmpLink.href = image;  

  document.body.appendChild( tmpLink );  
  tmpLink.click();  
  document.body.removeChild( tmpLink );  
}

function handleImage(event){
  var reader = new FileReader();
  reader.onload = function(event){
      var img = new Image();
      img.onload = function(){
          ctx.drawImage(img,0,0);
      }
      img.src = event.target.result;
  }
  reader.readAsDataURL(e.target.files[0]);     
}