# Software Studio 2021 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Rainbow pen                                      | 1~5%      | Y         |


---

### How to use 

    "Brush color" : decides the color painted out from brush and shape tools.
    "Brush size" : decides the size of brush, eraser and rainbow pen.
    text : type in the lattice, choose the text mode and click on the canvas, it will show the text with following font and size.
    "brush" "eraser" : do exactly as its name.
    "circle" "triangle" "square" : drag to paint corresponding shape.
    "undo" "redo" : do exactly as its name.
    "rainbow pen" : a pen that will change color with your mouse moving.
    "upload" : upload image and show on canvas.
    "download" : download the current canvas as a .png file
    "new page" : refresh the canvas.

### Function description

    rainbow pen : just select the rainbow pen mode and draw?

### Gitlab page link

    https://107061202.gitlab.io/AS_01_WebCanvas

### Others (Optional)

    Nothing special.

<style>
table th{
    width: 100%;
}
</style>

